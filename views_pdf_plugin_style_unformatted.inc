<?php
/**
 * @file
 * Unformatted PDF style
 */

/**
 * This class holds all the funtionality used for the unformatted style plugin.
 */
class views_pdf_plugin_style_unformatted extends views_plugin_style {

  /**
   * Attach this view to another display as a feed.
   * Provide basic functionality for all export style views like attaching a
   * feed image link.
   */
  function attach_to($display_id, $path, $title) {
    $display = $this->view->display[$display_id]->handler;
    $url_options = array();
    $input = $this->view->get_exposed_input();
    if ($input) {
      $url_options['query'] = $input;
    }

    $url = url($this->view->get_url(NULL, $path), $url_options);

    if (empty($this->view->feed_icon)) {
      $this->view->feed_icon = '';
    }
    $this->view->feed_icon .= theme('views_pdf_icon', $url, $title);
  }

}
